mkdir -p input outputLocal outputGlobal

docker run --user "$(id -u):$(id -g)" -v ${HOME}/POLAR/repos/rocker-project/input:/opt/input -v ${HOME}/POLAR/repos/rocker-project/outputGlobal:/opt/outputGlobal -v ${HOME}/POLAR/repos/rocker-project/outputLocal:/opt/outputLocal registry.gitlab.com/tpeschel/rocker-project:latest
